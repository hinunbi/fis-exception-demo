package com.shb.exception;

public class InvalidInputException extends Exception {
  public InvalidInputException(String s) {
    super(s);
  }
}
