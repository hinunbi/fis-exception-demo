package com.shb.exception;


import com.shb.model.ObjectFactory;
import com.shb.model.PrepayIdResponse;
import org.springframework.stereotype.Component;

@Component
public class ExceptionHandler {

  ObjectFactory objectFactory = new ObjectFactory();

  public PrepayIdResponse buildErrorResponse(Exception e) {

    PrepayIdResponse prepayIdResponse = objectFactory.createPrepayIdResponse();
    prepayIdResponse.setXml(objectFactory.createPrepayIdResponseXml());

    String returnCode = "000";
    String returnMessage = "Success";

    if (e instanceof InvalidInputException) {
      returnCode = "002";
      returnMessage = e.getMessage();
    } else {
      returnCode = "999";
      returnMessage = e.getMessage();
    }
    prepayIdResponse.getXml().setReturnCode(returnCode);
    prepayIdResponse.getXml().setReturnMsg(returnMessage);

    return prepayIdResponse;
  }
}
