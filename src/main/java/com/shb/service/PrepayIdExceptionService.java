package com.shb.service;

import com.shb.exception.InvalidInputException;
import com.shb.model.ObjectFactory;
import com.shb.model.PrepayIdRequest;
import com.shb.model.PrepayIdResponse;
import org.apache.commons.lang3.builder.MultilineRecursiveToStringStyle;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class PrepayIdExceptionService {

  private static final Logger logger = LoggerFactory.getLogger(PrepayIdExceptionService.class);

  @Autowired
  ObjectFactory objectFactory;

  Integer cnt = 0;

  @SuppressWarnings("unused")
  public PrepayIdResponse process(PrepayIdRequest prepayIdRequest) throws Exception {

    PrepayIdResponse prepayIdResponse = objectFactory.createPrepayIdResponse();
    prepayIdResponse.setXml(objectFactory.createPrepayIdResponseXml());

    logger.info("PrepayIdService.process called :{}",
        ReflectionToStringBuilder.toString(prepayIdRequest,
            new MultilineRecursiveToStringStyle()));

    switch (cnt++ % 2) {
      case 0:
        throw new InvalidInputException("Cause : InvalidInputException, cnt = " + cnt);
      case 1:
        throw new Exception("Cause : MCHID_NOT_EXIST, cnt = " + cnt);
    }
    prepayIdResponse.getXml().setReturnCode("000");
    prepayIdResponse.getXml().setReturnMsg("Success");

    return prepayIdResponse;
  }
}
