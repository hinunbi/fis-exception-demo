package com.shb.exception;

import com.shb.Application;
import com.shb.model.PrepayIdRequest;
import com.shb.model.PrepayIdResponse;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(CamelSpringBootRunner.class)
@SpringBootTest(classes = Application.class)
public class ExceptionHandlerTest {

  private static final Logger logger = LoggerFactory.getLogger(ExceptionHandlerTest.class);

  @Autowired
  CamelContext camelContext;

  @Autowired
  ProducerTemplate producer;

  @Test
  public void buildErrorResponse() {

    PrepayIdRequest prepayIdRequest =
        producer.requestBody("direct:loadSample",
            "", PrepayIdRequest.class);

    for (int i = 0; i < 10; i++) {
      PrepayIdResponse prepayIdResponse =
          producer.requestBody("direct:throwException",
              prepayIdRequest, PrepayIdResponse.class);

      logger.info("\n" + producer.requestBody("direct:marshalResopnse",
          prepayIdResponse, String.class));
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
      }

    }
  }
}